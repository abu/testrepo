# testrepo

This repo is just for testing purposes. It will be deleted afterwards. OK, maybe not.

> Wer nichts weiss, muss alles lernen.

## Lorem ipsum

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation *ullamco* laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum _dolore_ eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

- [Wiki](/wiki/https://codeberg.org/abu/testrepo.wiki/Lorem-ipsum) Absoluter Link geht nicht.
- [Wiki](https://codeberg.org/abu/testrepo/wiki/Lorem-ipsum) Relativer Link ist OK.

## Donations
If you want to support my work, you can donate via
[![Donate via LiberaPay](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/abu/donate)
or [PayPal](https://paypal.me/alfredbuehler).
