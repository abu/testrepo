---
name: Bug report
about: This template is only for bug reports
title: ''
ref: 'master'
labels:
  - bug
---

### Actual behavior

<!-- A clear and concise description of what the bug is. -->

### Expected behavior

<!-- A clear and concise description of what you expected to happen. -->

### Steps to reproduce

- ...
- ...

### Screenshots

<!-- If applicable, add screenshots to help explain your problem. -->

### Logs


### Configuration

- Kanboard version: <!-- provide exact version (not master or main) -->
- Plugin version:
- Database type and version:
- PHP version:
- OS:
- Browser:
